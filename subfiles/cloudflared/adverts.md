## How many % of adverts and trackers are using Cloudflare?


- [Easylist](https://web.archive.org/web/20210516110248/https://easylist.to/)
```
EasyList is the primary filter list that removes most adverts from international webpages, including unwanted frames, images and objects.

EasyPrivacy is an optional supplementary filter list that completely removes all forms of tracking from the internet, including web bugs, tracking scripts and information collectors, thereby protecting your personal data.
```


We picked domain-blocking lines from the list and filtered out domains which has exception rules.
Here's the result.


| Adblock list | Domains Count | Cloudflare | % |
| --- | --- | --- | --- |
| [EasyList](https://easylist.to/easylist/easylist.txt) | 29,944 | 8,291 | 27.7% |
| [EasyPrivacy](https://easylist.to/easylist/easyprivacy.txt) | 17,499 | 5,395 | 30.8% |
| [AdGuard](https://adguardteam.github.io/AdGuardSDNSFilter/Filters/filter.txt) | 46,193 | 8,813 | 19.1% |
| [AdAway](https://raw.githubusercontent.com/AdAway/adaway.github.io/master/hosts.txt) | 2,089 | 726 | 34.8% |
| Total | 65,323 | 16,691 | 25.6% |


### 25.6% of adverts and trackers are using Cloudflare.
## How many % of pornographic domains are using Cloudflare?


We downloaded the pornhosts list from [here](https://raw.githubusercontent.com/Sinfonietta/hostfiles/master/pornography-hosts) and filter out duplicates.
Here's the result.


[//]: # (start replacement)


- Top 5 TLDs

| TLD | Count |
| --- | --- |
| com | 9,407 |
| net | 1,240 |
| org | 340 |
| pro | 328 |
| tv | 267 |


- Cloudflare %

| Type | Count |
| --- | --- |
| Cloudflare | 5,810 |
| Normal | 8,691 |


### 40.1% of pornographic domains are using Cloudflare.